import csv
import datetime
import urllib.request
import time

fieldnames = ['timestamp', 'temp', 'humidity', 'dew point', 'pressure',
        'mean wind speed', 'average wind direction', 'sunshine', 'rainfall'
        'max wind speed']
url = 'https://www.cl.cam.ac.uk/research/dtg/weather/weather-raw.csv'
weather_file = urllib.request.urlopen(url)
read_file = weather_file.read()

with open(read_file, newline='') as w_file:
    reader = csv.DictReader(w_file, fieldnames=fieldnames, delimiter=',')

    timestamp = []
    temp = []
    rainfall = []
    weather = [timestamp, temp, rainfall]

    for i in reader:
        date_object = time.strptime(i[0], 'Y-%m-%d %H:%M:%S')
        timestamp.append(date_object)
        temp.append(i[1])
        rainfall.append(i[8])

    print(weather.shape)
    
    rainfall_total = 0
    jan_rain = 0
    jan_temp = 0
    feb_rain = 0
    feb_temp = 0
    mar_rain = 0
    mar_temp = 0
    apr_rain = 0
    apr_temp = 0
    may_rain = 0
    may_temp = 0
    jun_rain = 0
    jun_temp = 0
    jul_rain = 0
    jul_temp = 0
    aug_rain = 0
    aug_temp = 0
    sep_rain = 0
    sep_temp = 0
    oct_rain = 0
    oct_temp = 0
    nov_rain = 0
    nov_temp = 0
    dec_rain = 0
    dec_temp = 0
    jan = 0
    feb = 0
    mar =0
    apr = 0
    may = 0
    jun = 0
    jul = 0
    aug = 0
    sep = 0
    octo = 0
    nov = 0
    dec = 0
    for i in weather:
        month = i[0].tm_month
        if month == 0:
            jan = jan +1
            rainfall_total = rainfall_total + i[2]
            jan_rain = jan_rain + i[2]
            jan_temp = jan_temp + i[1]
        elif month == 1:
            feb = feb + 1
            rainfall_total = rainfall_total + i[2]
            feb_rain = feb_rain + i[2]
            feb_temp = feb_temp + i[1]
        elif month == 2:
            mar = mar +1
            rainfall_total = rainfall_total + i[2]
            mar_rain = mar_rain + i[2]
            mar_temp = mar_temp + i[1]
        elif momth == 3:
            apr = apr +1
            rainfall_total = rainfall_total + i[2]
            apr_rain = apr_rain + i[2]
            apr_temp = apr_temp + i[1]
        elif month == 4:
            may = may +1
            rainfall_total = rainfall_total + i[2]
            may_rain = may_rain + i[2]
            may_temp = may_temp + i[1]
        elif month == 5:
            jun = jun +1
            rainfall_total = rainfall_total + i[2]
            jun_rain = jun_rain + i[2]
            jun_temp = jun_temp + i[1]

        elif month == 6:
            jul = jul +1
            rainfall_total = rainfall_total + i[2]
            jul_rain = jul_rain + i[2]
            jul_temp = jul_temp + i[1]
        elif month == 7:
            aug = aug +1
            rainfall_total = rainfall_total + i[2]
            aug_rain = aug_rain + i[2]
            aug_temp = aug_temp + i[1]
        elif month == 8:
            sep = sep +1
            rainfall_total = rainfall_total + i[2]
            sep_rain = sep_rain + i[2]
            sep_temp = sep_temp + i[1]
        elif month == 9:
            octo = octo +1
            rainfall_total = rainfall_total + i[2]
            oct_rain = oct_rain + i[2]
            oct_temp = oct_temp + i[1]
        elif month == 10:
            nov = nov +1
            rainfall_total = rainfall_total + i[2]
            nov_rain = nov_rain + i[2]
            nov_temp = nov_temp + i[1]
        elif month == 11:
            dec = dec +1
            rainfall_total = rainfall_total + i[2]
            jan_rain = jan_rain + i[2]
            jan_temp = jan_temp + i[1]

    average_rainfall = (rainfall_total / (len(i[2])/27))/ 1000
    av_jan_temp = (jan_temp/jan) /10
    av_jan_rainfall = (jan_rain/jan) /1000
    
    av_feb_temp = (feb_temp/feb) /10
    av_feb_rainfall = (feb_rain/feb) /1000
    av_mar_temp = (mar_temp/mar) /10
    av_apr_temp = (apr_temp/apr) /10
    av_may_temp = (may_temp/may) /10
    av_jun_temp = (jun_temp/jun) /10
    av_jul_temp = (jul_temp/jul) /10
    av_aug_temp = (aug_temp/aug) /10
    av_sep_temp = (sep_temp/sep) /10
    av_oct_temp = (oct_temp/octo) /10
    av_nov_temp = (nov_temp/nov) /10
    av_dec_temp = (dec_temp/dec) /10
    av_mar_rainfall = (mar_rain/mar) /1000
    av_apr_rainfall = (apr_rain/apr) /1000
    av_may_rainfall = (may_rain/may) /1000
    av_jun_rainfall = (jun_rain/jun) /1000
    av_jul_rainfall = (jul_rain/jul) /1000
    av_aug_rainfall = (aug_rain/aug) /1000
    av_sep_rainfall = (sep_rain/sep) /1000
    av_oct_rainfall = (oct_rain/octo) /1000
    av_nov_rainfall = (nov_rain/nov) /1000
    av_dec_rainfall = (dec_rain/dec) /1000
    percentage_of_rain_in_summer = (av_apr_rainfall + av_may_rainfall + av_jun_rainfall + av_jul_rainfall + av_aug_rainfall + av_sep_rainfall)/6

    print('average annual rainfall = %s, rain in summer = %s, average january temp = %s, average feb temp = %s, avg mar temp = %s, avg may temp = %s, avg jun temp = %s, avg jul temp = %s, avg aug temp = %s, avg sep temp = %s, avg oct temp = %s, avg nov temp = %s, avg dec temp = %s', average_rainfall, percentage_of_rain_in_summer, av_jan_temp, av_feb_temp, av_mar_temp, av_apr_temp, av_may_temp, av_jun_temp, av_jul_temp, av_aug_temp, av_sep_temp, av_oct_temp, av_nov_temp, av_dec_temp)
