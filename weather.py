import csv
import datetime
import time

with open('weather-raw.csv', newline='') as csvfile:
    weather_reader = csv.reader(csvfile, delimiter = ',')

    years = []
    current_year = 0
    tally = 0
    for row in weather_reader:
        temp = 10
        date_object = time.strptime(row[0], "%Y-%m-%d %H:%M:%S")
        if date_object.tm_year != current_year:
            years.append([current_year, tally])
            current_year = date_object.tm_year
            tally = 0
        else:
            try:
                temp = int(row[1])
            except ValueError:
                temp = 10
            if temp >= 300:
                tally = tally + 1

    years.append([current_year, tally])
print(years)
